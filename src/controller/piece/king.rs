use crate::controller::movement::{Capture, Castling, Move, Quiet};
use crate::controller::movement::{
    KINGSIDE_BLACK_KING, KINGSIDE_BLACK_ROOK, KINGSIDE_WHITE_KING, KINGSIDE_WHITE_ROOK,
    QUEENSIDE_BLACK_ROOK, QUEENSIDE_WHITE_KING, QUEENSIDE_WHITE_ROOK,
};
use crate::controller::piece::{Piece, PieceBehaviour, PieceColour};
use crate::controller::{board::Board, movement::QUEENSIDE_BLACK_KING};
use crate::errors::EngineError;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct King {
    has_moved: bool,
}

impl PieceBehaviour for King {
    fn moves(
        &self,
        king: &Piece,
        board: &Board,
        turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError> {
        let mut moves = Vec::with_capacity(8 + 2);

        standard_moves(king, board, turn_player.clone(), &mut moves)?;
        if !self.has_moved {
            castling_moves(king, board, turn_player, &mut moves)?;
        }

        return Ok(moves);
    }
}

fn standard_moves(
    king: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let normed_offsets = [(1, 1), (-1, 1), (-1, -1), (1, -1)];
    for noffset in normed_offsets.iter() {
        // If OOB, next offset
        let absolute = match king.position.checked_add_offset(*noffset) {
            Ok(p) => p,
            Err(_) => continue,
        };

        match board.piece_at(&absolute)? {
            // Capturing move; continue
            Some(piece) => {
                if turn_player != piece.colour {
                    let movement = Capture {
                        position: piece.position().clone(),
                        new_position: absolute,
                    };
                    moves.push(movement.into());
                }
            }
            // Non capturing, continue
            None => {
                let movement = Quiet {
                    position: king.position().clone(),
                    new_position: absolute,
                };
                moves.push(movement.into());
            }
        }
    }

    return Ok(());
}

fn castling_moves(
    king: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let rooks: Vec<_> = board
        .iter()
        .filter_map(|r| {
            if r.colour.is_same_colour(&turn_player) {
                return None;
            }

            return r.piece_type.as_rook().map(|metadata| return (r, metadata));
        })
        .collect();

    // (new king position, new rook position, rook)
    let castling_moves: Vec<_> = rooks
        .into_iter()
        .filter_map(|(rook, metadata)| {
            if !metadata.has_moved() {
                return None;
            }
            let movement = match (turn_player.clone(), rook.position.x < king.position.x) {
                /* white queenside*/
                (PieceColour::White, true) => (
                    QUEENSIDE_WHITE_KING.clone(),
                    QUEENSIDE_WHITE_ROOK.clone(),
                    rook,
                ),
                /* black queenside */
                (PieceColour::Black, true) => (
                    QUEENSIDE_BLACK_KING.clone(),
                    QUEENSIDE_BLACK_ROOK.clone(),
                    rook,
                ),
                /* white kingside */
                (PieceColour::White, false) => (
                    KINGSIDE_WHITE_KING.clone(),
                    KINGSIDE_WHITE_ROOK.clone(),
                    rook,
                ),
                /* black kingside */
                (PieceColour::Black, false) => (
                    KINGSIDE_BLACK_KING.clone(),
                    KINGSIDE_BLACK_ROOK.clone(),
                    rook,
                ),
            };
            return Some(movement);
        })
        .collect();

    let castling_moves: Vec<Move> = castling_moves
        .into_iter()
        .map(|(new_king_pos, new_rook_pos, rook)| {
            return Castling {
                king_position: king.position.clone(),
                new_king_position: new_king_pos,

                rook_position: rook.position.clone(),
                new_rook_position: new_rook_pos,
            }
            .into();
        })
        .collect();

    moves.extend(castling_moves);

    return Ok(());
}
