use crate::controller::movement::{Capture, Move, Quiet};
use crate::controller::piece::{Piece, PieceBehaviour, PieceColour};
use crate::errors::EngineError;

use crate::controller::board::Board;

use crate::controller::position::mul_offset;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Bishop;

impl PieceBehaviour for Bishop {
    fn moves(
        &self,
        bishop: &Piece,
        board: &Board,
        _turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError> {
        let normed_offsets = [(1, 1), (-1, 1), (-1, -1), (1, -1)];
        let mut moves = Vec::with_capacity(normed_offsets.len() * 7);

        for noffset in normed_offsets.iter() {
            for coeff in 1..8 {
                let offset = mul_offset(*noffset, coeff);

                // If OOB, next offset
                let absolute = match bishop.position.checked_add_offset(offset) {
                    Ok(p) => p,
                    Err(_) => break,
                };

                match board.piece_at(&absolute)? {
                    // Capturing move; next offset after this
                    Some(piece) => {
                        if bishop.colour != piece.colour {
                            let movement = Capture {
                                position: piece.position().clone(),
                                new_position: absolute,
                            };
                            moves.push(movement.into());
                        }
                        break;
                    }
                    // Non capturing, continue
                    None => {
                        let movement = Quiet {
                            position: bishop.position().clone(),
                            new_position: absolute,
                        };
                        moves.push(movement.into());
                    }
                }
            }
        }

        return Ok(moves);
    }
}
