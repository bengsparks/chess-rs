use crate::controller::movement::{Capture, Move, Quiet};
use crate::controller::piece::Board;
use crate::controller::piece::Piece;
use crate::controller::piece::PieceBehaviour;
use crate::controller::piece::PieceColour;
use crate::controller::position::mul_offset;
use crate::errors::EngineError;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Rook {
    has_moved: bool,
}

impl Rook {
    pub fn has_moved(&self) -> bool {
        return self.has_moved;
    }

    pub fn has_moved_mut(&mut self) -> &mut bool {
        return &mut self.has_moved;
    }
}

impl PieceBehaviour for Rook {
    fn moves(
        &self,
        rook: &Piece,
        board: &Board,
        turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError> {
        let normed_offsets = [(1, 0), (0, 1), (-1, 0), (0, -1)];
        let mut moves = Vec::with_capacity(normed_offsets.len() * 7);

        for noffset in normed_offsets.iter() {
            for coeff in 1..8 {
                let offset = mul_offset(*noffset, coeff);

                // If OOB, next offset
                let absolute = match rook.position.checked_add_offset(offset) {
                    Ok(p) => p,
                    Err(_) => break,
                };

                match board.piece_at(&absolute)? {
                    // Capturing move; next offset after this
                    Some(piece) => {
                        if turn_player != piece.colour {
                            let movement = Capture {
                                position: piece.position().clone(),
                                new_position: absolute,
                            };
                            moves.push(movement.into());
                        }
                        break;
                    }
                    // Non capturing, continue
                    None => {
                        let movement = Quiet {
                            position: rook.position().clone(),
                            new_position: absolute,
                        };
                        moves.push(movement.into());
                    }
                }
            }
        }

        return Ok(moves);
    }
}
