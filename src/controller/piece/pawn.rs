use crate::controller::movement::{Capture, EnPassant, Move, Quiet};
use crate::controller::piece::Board;
use crate::controller::piece::Piece;
use crate::controller::piece::PieceBehaviour;
use crate::controller::piece::PieceColour;
use crate::errors::EngineError;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Pawn {
    is_first_move: bool,
    is_en_passant_available: bool,
    can_en_passant_attack: bool,
}

impl PieceBehaviour for Pawn {
    fn moves(
        &self,
        pawn: &Piece,
        board: &Board,
        turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError> {
        let mut moves = Vec::with_capacity(3 * 2);

        single_move(pawn, board, turn_player.clone(), &mut moves)?;
        capturing_moves(pawn, board, turn_player.clone(), &mut moves)?;
        if self.is_first_move {
            double_move(pawn, board, turn_player.clone(), &mut moves)?;
        }

        if self.is_en_passant_available {
            en_passant(pawn, board, turn_player, &mut moves)?;
        }

        return Ok(moves);
    }
}

fn single_move(
    pawn: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let offset = (
        0,
        match turn_player {
            PieceColour::White => 1,
            PieceColour::Black => -1,
        },
    );
    let move_position = pawn
        .position
        .checked_add_offset(offset)
        .expect("Pawn has moved off the board");

    // Cannot capture moving forward
    if board.piece_at(&move_position)?.is_none() {
        moves.push(
            Quiet {
                position: pawn.position.clone(),
                new_position: move_position,
            }
            .into(),
        );
    }

    return Ok(());
}

fn double_move(
    pawn: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let offset = (
        0,
        match turn_player {
            PieceColour::White => 2,
            PieceColour::Black => -2,
        },
    );
    let move_position = pawn
        .position
        .checked_add_offset(offset)
        .expect("Pawn has moved off the board");

    // Cannot capture moving forward
    if board.piece_at(&move_position)?.is_none() {
        moves.push(
            Quiet {
                position: pawn.position.clone(),
                new_position: move_position,
            }
            .into(),
        );
    }

    return Ok(());
}

fn capturing_moves(
    pawn: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let y_direction = match turn_player {
        PieceColour::White => 1,
        PieceColour::Black => -1,
    };

    let offsets = [(-1, y_direction), (1, y_direction)];

    for offset in offsets.iter() {
        let move_position = match pawn.position.checked_add_offset(*offset) {
            Ok(p) => p,
            Err(_) => continue,
        };

        match board.piece_at(&pawn.position)? {
            Some(_) => {
                let capture = Capture {
                    position: pawn.position.clone(),
                    new_position: move_position,
                };
                moves.push(capture.into());
            }
            None => continue,
        };
    }

    return Ok(());
}

fn en_passant(
    pawn: &Piece,
    board: &Board,
    turn_player: PieceColour,
    moves: &mut Vec<Move>,
) -> Result<(), EngineError> {
    let y_direction = match turn_player {
        PieceColour::White => 1,
        PieceColour::Black => -1,
    };

    let move_offsets = [(-1, y_direction), (1, y_direction)];
    let enemy_position_offsets = [(-1, 0), (1, 0)];

    for (move_offset, enemy_offset) in move_offsets.iter().zip(enemy_position_offsets.iter()) {
        let move_position = match pawn.position.checked_add(move_offset) {
            Ok(p) => p,
            Err(_) => continue,
        };

        let enemy_position = match pawn.position.checked_add(enemy_offset) {
            Ok(p) => p,
            Err(_) => continue,
        };

        match board.piece_at(&enemy_position)? {
            Some(_p) => {
                let ep = EnPassant {
                    position: pawn.position.clone(),
                    new_position: move_position,
                    captured_pawn: enemy_position.clone(),
                };
                moves.push(ep.into());
            }
            None => continue,
        };
    }

    return Ok(());
}
