use crate::controller::board::Board;
use crate::controller::movement::{Capture, Move, Quiet};
use crate::controller::piece::PieceColour;
use crate::controller::piece::{Piece, PieceBehaviour};
use crate::errors::EngineError;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Knight;

impl PieceBehaviour for Knight {
    fn moves(
        &self,
        knight: &Piece,
        board: &Board,
        _turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError> {
        let offsets = [
            (2, 1),
            (2, -1),
            (-2, 1),
            (-2, -1),
            (1, 2),
            (-1, 2),
            (1, -2),
            (-1, -2),
        ];
        let mut moves = Vec::with_capacity(offsets.len());

        for offset in offsets.iter() {
            // If OOB, ignore and continue
            let absolute = match knight.position.checked_add_offset(*offset) {
                Ok(p) => p,
                Err(_) => continue,
            };

            match board.piece_at(&absolute)? {
                // Capturing move; next offset after this
                Some(piece) => {
                    if knight.colour != piece.colour {
                        let movement = Capture {
                            position: piece.position().clone(),
                            new_position: absolute,
                        };
                        moves.push(movement.into());
                    }
                    continue;
                }
                // Non capturing, continue
                None => {
                    let movement = Quiet {
                        position: knight.position().clone(),
                        new_position: absolute,
                    };
                    moves.push(movement.into());
                }
            }
        }

        return Ok(moves);
    }
}
