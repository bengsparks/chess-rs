use crate::controller::board::Board;
use crate::controller::colour::PieceColour;
use crate::controller::movement::Move;
use crate::controller::position::Position;
use crate::controller::types::PieceType;
use crate::errors::EngineError;

pub mod bishop;
pub mod king;
pub mod knight;
pub mod pawn;
pub mod queen;
pub mod rook;

// mod tests;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Piece {
    position: Position,
    colour: PieceColour,
    piece_type: PieceType,
}

impl Piece {
    pub fn new(position: Position, colour: PieceColour, piece_type: PieceType) -> Self {
        return Self {
            position,
            colour,
            piece_type,
        };
    }

    /// Get a mutable reference to the piece's position.
    pub fn position_mut(&mut self) -> &mut Position {
        return &mut self.position;
    }

    /// Get a reference to the piece's position.
    pub fn position(&self) -> &Position {
        return &self.position;
    }

    /// Get a reference to the piece's piece type.
    pub fn piece_type(&self) -> &PieceType {
        return &self.piece_type;
    }

    /// Get a mutable reference to the piece's piece type
    pub fn piece_type_mut(&mut self) -> &mut PieceType {
        return &mut self.piece_type;
    }

    /// Get a reference to the piece's colour.
    pub fn colour(&self) -> &PieceColour {
        return &self.colour;
    }

    pub fn moves(&self, board: &Board, turn_player: PieceColour) -> Result<Vec<Move>, EngineError> {
        return match self.piece_type {
            PieceType::Bishop(ref p) => p.moves(self, board, turn_player),
            PieceType::King(ref p) => p.moves(self, board, turn_player),
            PieceType::Knight(ref p) => p.moves(self, board, turn_player),
            PieceType::Pawn(ref p) => p.moves(self, board, turn_player),
            PieceType::Queen(ref p) => p.moves(self, board, turn_player),
            PieceType::Rook(ref p) => p.moves(self, board, turn_player),
        };
    }
}

trait PieceBehaviour {
    fn moves(
        &self,
        piece: &Piece,
        board: &Board,
        turn_player: PieceColour,
    ) -> Result<Vec<Move>, EngineError>;
}
