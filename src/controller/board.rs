use crate::controller::board::state::GameContext;
use crate::controller::movement::{Capture, Castling, EnPassant, Move, Promotion, Quiet};
use std::{convert::TryInto, usize};

use crate::{controller::piece::Piece, errors::EngineError};

use super::{colour::PieceColour, position::Position, square::Square};

pub mod state;

pub mod check;
pub mod checkmate;
pub mod normal;

pub const BOARD_DIM: i8 = 8;
pub const BOARD_AREA: i8 = BOARD_DIM * BOARD_DIM;

#[derive(Clone)]
pub struct Board {
    squares: Vec<Square>,
    white_taken: Vec<Piece>,
    black_taken: Vec<Piece>,
    game_context: GameContext,
}

impl Default for Board {
    fn default() -> Self {
        let squares = Vec::with_capacity(BOARD_AREA as usize);

        return Board {
            squares,
            white_taken: Vec::with_capacity(BOARD_DIM as usize * 2),
            black_taken: Vec::with_capacity(BOARD_DIM as usize * 2),
            game_context: GameContext::normal(PieceColour::White),
        };
    }
}

impl Board {
    pub fn new() -> Self {
        return Default::default();
    }

    pub fn with_state(pieces: Vec<Piece>, context: GameContext) -> Result<Self, EngineError> {
        let mut squares = Vec::with_capacity(BOARD_AREA as usize);
        for piece in pieces {
            let index: usize = piece.position().try_into()?;
            squares[index] = Square::with_piece(piece);
        }

        return Ok(Board {
            squares,
            game_context: context,
            white_taken: vec![],
            black_taken: vec![],
        });
    }

    pub(crate) fn piece_at(&self, position: &Position) -> Result<Option<&Piece>, EngineError> {
        let index: usize = position.try_into()?;
        return self.piece_at_index(index);
    }

    pub(crate) fn piece_at_index(
        &self,
        index: impl Into<usize>,
    ) -> Result<Option<&Piece>, EngineError> {
        let piece = self.squares[index.into()].piece();
        return Ok(Option::from(piece));
    }

    pub(crate) fn piece_at_mut(
        &mut self,
        position: &Position,
    ) -> Result<Option<&mut Piece>, EngineError> {
        let index: usize = position.try_into()?;
        return self.piece_at_index_mut(index);
    }

    pub(crate) fn piece_at_index_mut(
        &mut self,
        index: impl Into<usize>,
    ) -> Result<Option<&mut Piece>, EngineError> {
        let piece = self.squares[index.into()].piece_mut();
        return Ok(Option::from(piece));
    }

    pub(crate) fn apply(mut self, movement: Move) -> Result<Board, EngineError> {
        match movement {
            Move::Quiet(Quiet {
                position,
                new_position,
            }) => {
                let piece = self.remove_piece(&position)?;
                self = self.place(piece, new_position)?;
            }

            Move::Promotion(Promotion {
                pawn_position,
                new_position: new_pawn_position,
                promoted_to,
            }) => {
                let mut piece = self.remove_piece(&pawn_position)?;
                *piece.piece_type_mut() = promoted_to;

                if self.piece_at(&new_pawn_position).is_ok() {
                    self = self.capture(new_pawn_position.clone())?;
                }

                self = self.place(piece, new_pawn_position)?;
            }

            Move::Castling(Castling {
                king_position,
                new_king_position,
                rook_position,
                new_rook_position,
            }) => {
                let rook = self.remove_piece(&rook_position)?;
                let king = self.remove_piece(&king_position)?;

                self = self.place(rook, new_rook_position)?;
                self = self.place(king, new_king_position)?;
            }

            Move::Capture(Capture {
                position,
                new_position,
            }) => {
                let piece = self.remove_piece(&position)?;

                self = self.capture(new_position)?;
                self = self.place(piece, position)?;
            }

            Move::EnPassant(EnPassant {
                position,
                new_position,
                captured_pawn,
            }) => {
                let piece = self.remove_piece(&position)?;
                self = self.capture(captured_pawn)?;
                self = self.place(piece, new_position)?;
            }
        }

        return Ok(self);
    }

    pub fn remove_piece(&mut self, position: &Position) -> Result<Piece, EngineError> {
        let index: usize = position.try_into()?;

        let square = self.squares.get_mut(index).ok_or_else(|| {
            return EngineError::NoPieceFound {
                position: position.into(),
            };
        })?;

        return Ok(square
            .replace(Square::empty())
            .expect("Expected piece on board"));
    }

    fn place(mut self, piece: Piece, target_position: Position) -> Result<Board, EngineError> {
        let old_index: usize = piece.position().try_into()?;
        let new_index: usize = target_position.clone().try_into()?;

        self.squares[old_index] = Square::empty();
        self.squares[new_index].set_piece(piece, target_position);

        return Ok(self);
    }

    fn capture(mut self, position: Position) -> Result<Board, EngineError> {
        let piece = self.remove_piece(&position)?;
        let captured_by = piece.colour().inverse();

        let taken_storage = match captured_by {
            PieceColour::White => &mut self.black_taken,
            PieceColour::Black => &mut self.white_taken,
        };

        taken_storage.push(piece);
        return Ok(self);
    }

    pub fn iter(&self) -> BoardIter {
        return BoardIter {
            squares: self.squares.iter(),
        };
    }
}

pub struct BoardIter<'a> {
    squares: std::slice::Iter<'a, Square>,
}

impl<'a> Iterator for BoardIter<'a> {
    type Item = &'a Piece;

    fn next(&mut self) -> Option<Self::Item> {
        // TODO: Consider rewriting with skip_while or similar
        while let Some(square) = self.squares.next() {
            match square.piece() {
                so @ Some(_) => return so.as_ref(),
                None => continue,
            }
        }

        return None;
    }
}
