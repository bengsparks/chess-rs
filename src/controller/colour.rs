#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum PieceColour {
    White,
    Black,
}

impl PieceColour {
    /// Returns `true` if the piece_colour is [`White`].
    pub fn is_white(&self) -> bool {
        return matches!(self, Self::White);
    }

    /// Returns `true` if the piece_colour is [`Black`].
    pub fn is_black(&self) -> bool {
        return matches!(self, Self::Black);
    }

    pub fn is_same_colour(&self, colour: &PieceColour) -> bool {
        return match colour {
            Self::White => self.is_white(),
            Self::Black => self.is_black(),
        };
    }

    pub fn is_different_colour(&self, colour: &PieceColour) -> bool {
        return match colour {
            Self::Black => self.is_white(),
            Self::White => self.is_black(),
        };
    }

    pub fn inverse(&self) -> PieceColour {
        return match self {
            Self::White => Self::Black,
            Self::Black => Self::White,
        };
    }
}
