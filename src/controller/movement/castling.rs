// Castling
use crate::controller::movement::Move;
use crate::controller::position::Position;

#[derive(Clone, Debug)]
pub struct Castling {
    pub king_position: Position,
    pub new_king_position: Position,

    pub rook_position: Position,
    pub new_rook_position: Position,
}

impl From<Castling> for Move {
    fn from(v: Castling) -> Self {
        return Self::Castling(v);
    }
}

// Castling positions
pub static QUEENSIDE_WHITE_KING: Position = Position::new((2, 0));
pub static QUEENSIDE_WHITE_ROOK: Position = Position::new((3, 0));

pub static KINGSIDE_WHITE_KING: Position = Position::new((6, 0));
pub static KINGSIDE_WHITE_ROOK: Position = Position::new((5, 0));

pub static QUEENSIDE_BLACK_KING: Position = Position::new((2, 7));
pub static QUEENSIDE_BLACK_ROOK: Position = Position::new((3, 7));

pub static KINGSIDE_BLACK_KING: Position = Position::new((6, 7));
pub static KINGSIDE_BLACK_ROOK: Position = Position::new((5, 7));
