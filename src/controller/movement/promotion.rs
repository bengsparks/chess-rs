// Pawn promotion
use crate::controller::movement::Move;
use crate::controller::position::Position;
use crate::controller::types::PieceType;

#[derive(Clone, Debug)]
pub struct Promotion {
    pub pawn_position: Position,
    pub new_position: Position,
    pub promoted_to: PieceType,
}

impl From<Promotion> for Move {
    fn from(v: Promotion) -> Self {
        return Self::Promotion(v);
    }
}
