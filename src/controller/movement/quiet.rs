// Quiet moves
use crate::controller::movement::Move;
use crate::controller::position::Position;

#[derive(Clone, Debug)]
pub struct Quiet {
    pub position: Position,
    pub new_position: Position,
}

impl From<Quiet> for Move {
    fn from(v: Quiet) -> Self {
        return Self::Quiet(v);
    }
}
