use crate::controller::movement::Move;
use crate::controller::position::Position;

#[derive(Clone, Debug)]
pub struct EnPassant {
    pub position: Position,
    pub new_position: Position,
    pub captured_pawn: Position,
}

impl From<EnPassant> for Move {
    fn from(v: EnPassant) -> Self {
        return Self::EnPassant(v);
    }
}
