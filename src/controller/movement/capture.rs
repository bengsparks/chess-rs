// Capture moves
use crate::controller::movement::Move;
use crate::controller::position::Position;

#[derive(Clone, Debug)]
pub struct Capture {
    pub position: Position,
    pub new_position: Position,
}

impl From<Capture> for Move {
    fn from(v: Capture) -> Self {
        return Self::Capture(v);
    }
}
