use crate::errors::EngineError;
use std::{
    convert::{TryFrom, TryInto},
    ops::Range,
};

use super::board::{BOARD_AREA, BOARD_DIM};

static IN_RANGE: Range<i8> = 0..BOARD_DIM;

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct Position {
    pub x: i8,
    pub y: i8,
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct Offset {
    x: i8,
    y: i8,
}

impl Position {
    pub const fn new((x, y): (i8, i8)) -> Self {
        return Position { x, y };
    }

    pub fn checked_add(
        &self,
        rhs: impl TryInto<Position, Error = EngineError>,
    ) -> Result<Self, EngineError> {
        let rhs: Position = rhs.try_into()?;

        let result = Position {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        };

        return in_bounds(self, &rhs, result);
    }

    pub fn checked_add_offset(&self, rhs: (i8, i8)) -> Result<Self, EngineError> {
        let result = Position {
            x: self.x + rhs.0,
            y: self.y + rhs.1,
        };

        return in_bounds(self, rhs, result);
    }

    pub fn checked_sub(
        &self,
        rhs: impl TryInto<Position, Error = EngineError>,
    ) -> Result<Self, EngineError> {
        let rhs: Position = rhs.try_into()?;

        let result = Position {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        };

        return in_bounds(self, &rhs, result);
    }

    pub fn checked_sub_offset(&self, rhs: (i8, i8)) -> Result<Self, EngineError> {
        let result = Position {
            x: self.x - rhs.0,
            y: self.y - rhs.1,
        };

        return in_bounds(self, rhs, result);
    }

    pub fn checked_mul(&self, rhs: i8) -> Result<Self, EngineError> {
        let result = Position {
            x: self.x * rhs,
            y: self.y * rhs,
        };

        if !IN_RANGE.contains(&result.x) || !IN_RANGE.contains(&result.y) {
            return Err(EngineError::OutOfBoundsPositionScalarResult {
                lhs: (self.x, self.y),
                rhs,
                result: result.into(),
            });
        }

        return Ok(result);
    }

    pub fn inbetween_exclusive(&self, other: &Position) -> Result<Vec<Position>, EngineError> {
        if self == other {
            return Ok(vec![]);
        }

        // horizontal
        let moves = if self.x == other.x {
            let (s, g) = ordered(self.y, other.y);
            (s + 1..g)
                .map(|y| return Position::new((self.x, y)))
                .collect::<Vec<_>>()
        }
        // vertical
        else if self.y == other.y {
            let (s, g) = ordered(self.x, other.x);
            (s + 1..g)
                .map(|x| return Position::new((x, self.y)))
                .collect::<Vec<_>>()
        }
        // diagonal
        else if self.x - other.x == self.y - other.y {
            let (sx, gx) = ordered(self.x, other.x);
            let (sy, gy) = ordered(self.y, other.y);

            (sx + 1..gx)
                .zip(sy + 1..gy)
                .map(Position::new)
                .collect::<Vec<_>>()
        } else {
            return Err(EngineError::IncompatibleAxis {
                lhs: self.into(),
                rhs: other.into(),
            });
        };

        return Ok(moves);
    }
}

fn ordered(lhs: i8, rhs: i8) -> (i8, i8) {
    return if lhs < rhs { (lhs, rhs) } else { (rhs, lhs) };
}

impl From<Position> for (i8, i8) {
    fn from(position: Position) -> Self {
        return (position.x, position.y);
    }
}

impl From<&Position> for (i8, i8) {
    fn from(position: &Position) -> Self {
        return (position.x, position.y);
    }
}

impl<'a> TryFrom<&'a (i8, i8)> for Position {
    type Error = EngineError;

    fn try_from(coord: &'a (i8, i8)) -> Result<Self, Self::Error> {
        if !IN_RANGE.contains(&coord.0) || !IN_RANGE.contains(&coord.1) {
            return Err(EngineError::OutOfBoundsPosition { position: *coord });
        }
        return Ok(Position {
            x: coord.0,
            y: coord.1,
        });
    }
}

pub fn checked_add(
    lhs: impl TryInto<Position, Error = EngineError>,
    rhs: impl TryInto<Position, Error = EngineError>,
) -> Result<Position, EngineError> {
    let lhs: Position = lhs.try_into()?;
    return Position::checked_add(&lhs, rhs);
}

pub fn checked_sub(
    lhs: impl TryInto<Position, Error = EngineError>,
    rhs: impl TryInto<Position, Error = EngineError>,
) -> Result<Position, EngineError> {
    let lhs: Position = lhs.try_into()?;
    return Position::checked_sub(&lhs, rhs);
}

pub fn checked_mul(
    lhs: impl TryInto<Position, Error = EngineError>,
    rhs: i8,
) -> Result<Position, EngineError> {
    let lhs: Position = lhs.try_into()?;

    return Position::checked_mul(&lhs, rhs);
}

pub fn mul_offset(lhs: (i8, i8), rhs: i8) -> (i8, i8) {
    return (lhs.0 * rhs, lhs.1 * rhs);
}

fn in_bounds(
    lhs: &Position,
    rhs: impl Into<(i8, i8)>,
    result: Position,
) -> Result<Position, EngineError> {
    if !IN_RANGE.contains(&result.x) || !IN_RANGE.contains(&result.y) {
        let lhs: (i8, i8) = lhs.into();
        let rhs: (i8, i8) = rhs.into();

        return Err(EngineError::OutOfBoundsPositionResult {
            lhs,
            rhs,
            result: result.into(),
        });
    }

    return Ok(result);
}

impl TryFrom<Position> for usize {
    type Error = EngineError;

    fn try_from(pos: Position) -> Result<Self, Self::Error> {
        let index = (pos.x * BOARD_DIM) as usize + pos.y as usize;
        if index < BOARD_AREA as usize {
            return Ok(index);
        }

        return Err(EngineError::OutOfBoundsPosition {
            position: pos.into(),
        });
    }
}

impl TryFrom<&Position> for usize {
    type Error = EngineError;

    fn try_from(pos: &Position) -> Result<Self, Self::Error> {
        let index = (pos.x * BOARD_DIM) as usize + pos.y as usize;
        if index < BOARD_AREA as usize {
            return Ok(index);
        }

        return Err(EngineError::OutOfBoundsPosition {
            position: pos.clone().into(),
        });
    }
}
