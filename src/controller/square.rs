use super::{piece::Piece, position::Position};

#[derive(Clone)]
pub struct Square {
    piece: Option<Piece>,
}

impl Square {
    pub fn empty() -> Self {
        return Square { piece: None };
    }

    pub fn with_piece(piece: Piece) -> Self {
        return Square { piece: Some(piece) };
    }

    /// Get a mutable reference to the square's piece.
    pub fn piece_mut(&mut self) -> &mut Option<Piece> {
        return &mut self.piece;
    }

    /// Get a reference to the square's piece.
    pub fn piece(&self) -> &Option<Piece> {
        return &self.piece;
    }

    /// Set the square's piece.
    pub fn set_piece(&mut self, mut piece: Piece, position: Position) {
        *piece.position_mut() = position;
        self.piece = Some(piece);
    }

    pub fn replace(&mut self, square: Square) -> Option<Piece> {
        return std::mem::replace(&mut self.piece, square.piece);
    }
}

