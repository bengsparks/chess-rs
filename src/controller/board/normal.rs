

use super::{state::GameState};


#[derive(Clone)]
pub struct Normal;

impl From<Normal> for GameState {
    fn from(v: Normal) -> Self {
        return Self::NonConstraining(v);
    }
}

