

use super::state::GameState;
use crate::controller::position::Position;

#[derive(Clone)]
pub struct Check {
    endangered_king: Position
}

impl Check {
    pub fn new(king_in_check: Position) -> Self {
        return Check { endangered_king: king_in_check };
    }
}


impl<'a> From<Check> for GameState {
    fn from(v: Check) -> Self {
        return Self::Check(v);
    }
}
