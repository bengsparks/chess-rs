
use crate::controller::position::Position;

use super::state::GameState;

#[derive(Clone)]
pub struct Checkmate {
    endangered_king: Position,
}

impl From<Checkmate> for GameState {
    fn from(v: Checkmate) -> Self {
        return Self::Checkmate(v);
    }
}