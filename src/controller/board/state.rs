use crate::controller::colour::PieceColour;

use super::{check::Check, checkmate::Checkmate, normal::Normal};

#[derive(Clone)]
pub struct GameContext {
    turn_player_colour: PieceColour,
    state: GameState
}

impl<'a> GameContext {
    pub fn new(turn_player_colour: PieceColour, state: GameState) -> Self {
        return Self {
            turn_player_colour, state
        };
    }

    pub fn normal(turn_player_colour: PieceColour) -> Self {
        return GameContext {
            turn_player_colour,
            state: Normal.into()
        }
    }

    /// Get a mutable reference to the game context's turn player.
    pub fn turn_player_mut(&mut self) -> &mut PieceColour {
        return &mut self.turn_player_colour;
    }

    /// Get a reference to the game context's turn player.
    pub fn turn_player(&self) -> &PieceColour {
        return &self.turn_player_colour;
    }

    /// Get a mutable reference to the game context's state.
    pub fn state_mut(&mut self) -> &mut GameState {
        return &mut self.state;
    }

    /// Get a reference to the game context's state.
    pub fn state(&self) -> &GameState {
        return &self.state;
    }
}

#[derive(Clone)]
pub enum GameState {
    NonConstraining(Normal),
    Check(Check),
    Checkmate(Checkmate),
}

impl GameState {
    /// Returns `true` if the game_state is [`NonConstraining`].
    pub fn is_non_constraining(&self) -> bool {
        return matches!(self, Self::NonConstraining(..));
    }

    /// Returns `true` if the game_state is [`Check`].
    pub fn is_check(&self) -> bool {
        return matches!(self, Self::Check(..));
    }

    /// Returns `true` if the game_state is [`Checkmate`].
    pub fn is_checkmate(&self) -> bool {
        return matches!(self, Self::Checkmate(..));
    }

    pub fn try_into_non_constraining(self) -> Result<Normal, Self> {
        if let Self::NonConstraining(v) = self {
            return Ok(v)
        } else {
            return Err(self)
        }
    }

    pub fn try_into_check(self) -> Result<Check, Self> {
        if let Self::Check(v) = self {
            return Ok(v)
        } else {
            return Err(self)
        }
    }

    pub fn try_into_checkmate(self) -> Result<Checkmate, Self> {
        if let Self::Checkmate(v) = self {
            return Ok(v)
        } else {
            return Err(self)
        }
    }
}



