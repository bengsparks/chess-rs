use crate::controller::board::Board;
use crate::controller::movement::{Capture, Move, Promotion};

use std::collections::HashMap;

use crate::controller::{colour::PieceColour, position::Position};
use crate::errors::EngineError;

pub struct BoardContext {
    board: Board,
    moves: HashMap<Position, Vec<Move>>,
    turn_player: PieceColour,
}

impl BoardContext {
    pub fn new(board: Board, turn_player: PieceColour) -> Result<Self, EngineError> {
        let mut moves = HashMap::with_capacity(218);

        for piece in board.iter() {
            if piece.colour().is_different_colour(&turn_player) {
                continue;
            }

            let all_moves = piece.moves(&board, turn_player)?;

            let mut valid_moves = Vec::with_capacity(all_moves.len());
            for movement in all_moves {
                if BoardContext::is_valid_move(&board, &movement, &turn_player)? {
                    valid_moves.push(movement);
                }
            }

            moves.insert(piece.position().clone(), valid_moves);
        }

        let context = BoardContext {
            board,
            moves,
            turn_player,
        };

        return Ok(context);
    }

    pub fn attacks_position(&self, position: &Position) -> HashMap<Position, &Vec<Move>> {
        let mut moves: HashMap<Position, &Vec<Move>> = HashMap::with_capacity(self.moves.len());

        for (map_position, pmoves) in self.moves.iter() {
            for movement in pmoves {
                if movement.attacks_position(position) {
                    moves.insert(map_position.clone(), pmoves);
                }
            }
        }

        return moves;
    }

    fn is_valid_move(
        board: &Board,
        movement: &Move,
        colour: &PieceColour,
    ) -> Result<bool, EngineError> {
        let board = board.clone().apply(movement.clone())?;

        let king = board
            .iter()
            .find(|piece| {
                return piece.piece_type().is_king() && piece.colour().is_same_colour(&colour);
            })
            .expect(format!("Could not find {:?} king", colour).as_str());

        for piece in board.iter() {
            // enemy pieces only
            if piece.colour().is_same_colour(&colour) {
                continue;
            }

            let captures_king = piece
                .moves(&board, colour.clone())?
                .iter()
                .find(|movement| match movement {
                    Move::Capture(Capture { new_position, .. })
                    | Move::Promotion(Promotion { new_position, .. }) => {
                        return new_position == king.position()
                    }
                    _ => return false,
                })
                .is_some();
            if captures_king {
                return Ok(true);
            }
        }

        return Ok(false);
    }
}
