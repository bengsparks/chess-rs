use super::piece::{
    bishop::Bishop, king::King, knight::Knight, pawn::Pawn, queen::Queen, rook::Rook,
};

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum PieceType {
    Bishop(Bishop),
    King(King),
    Knight(Knight),
    Pawn(Pawn),
    Queen(Queen),
    Rook(Rook),
}

impl PieceType {
    /// Returns `true` if the piece_type is [`Bishop`].
    pub fn is_bishop(&self) -> bool {
        return matches!(self, Self::Bishop(..));
    }

    /// Returns `true` if the piece_type is [`King`].
    pub fn is_king(&self) -> bool {
        return matches!(self, Self::King(..));
    }

    /// Returns `true` if the piece_type is [`Knight`].
    pub fn is_knight(&self) -> bool {
        return matches!(self, Self::Knight(..));
    }

    /// Returns `true` if the piece_type is [`Pawn`].
    pub fn is_pawn(&self) -> bool {
        return matches!(self, Self::Pawn(..));
    }

    /// Returns `true` if the piece_type is [`Queen`].
    pub fn is_queen(&self) -> bool {
        return matches!(self, Self::Queen(..));
    }

    /// Returns `true` if the piece_type is [`Rook`].
    pub fn is_rook(&self) -> bool {
        return matches!(self, Self::Rook(..));
    }

    pub fn as_bishop(&self) -> Option<&Bishop> {
        return if let Self::Bishop(v) = self {
            Some(v)
        } else {
            None
        };
    }

    pub fn as_king(&self) -> Option<&King> {
        return if let Self::King(v) = self {
            Some(v)
        } else {
            None
        };
    }

    pub fn as_knight(&self) -> Option<&Knight> {
        return if let Self::Knight(v) = self {
            Some(v)
        } else {
            None
        };
    }

    pub fn as_pawn(&self) -> Option<&Pawn> {
        return if let Self::Pawn(v) = self {
            Some(v)
        } else {
            None
        };
    }

    pub fn as_queen(&self) -> Option<&Queen> {
        return if let Self::Queen(v) = self {
            Some(v)
        } else {
            None
        };
    }

    pub fn as_rook(&self) -> Option<&Rook> {
        return if let Self::Rook(v) = self {
            Some(v)
        } else {
            None
        };
    }
}

impl From<Rook> for PieceType {
    fn from(v: Rook) -> Self {
        return Self::Rook(v);
    }
}

impl From<Queen> for PieceType {
    fn from(v: Queen) -> Self {
        return Self::Queen(v);
    }
}

impl From<Pawn> for PieceType {
    fn from(v: Pawn) -> Self {
        return Self::Pawn(v);
    }
}

impl From<Knight> for PieceType {
    fn from(v: Knight) -> Self {
        return Self::Knight(v);
    }
}

impl From<King> for PieceType {
    fn from(v: King) -> Self {
        return Self::King(v);
    }
}

impl From<Bishop> for PieceType {
    fn from(v: Bishop) -> Self {
        return Self::Bishop(v);
    }
}
