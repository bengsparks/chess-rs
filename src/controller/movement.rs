mod capture;
mod castling;
mod en_passant;
mod promotion;
mod quiet;

use crate::controller::position::Position;

pub use capture::Capture;
pub use castling::*;
pub use en_passant::EnPassant;
pub use promotion::Promotion;
pub use quiet::Quiet;

#[derive(Clone, Debug)]
pub enum Move {
    Quiet(Quiet),
    Capture(Capture),
    Promotion(Promotion),
    Castling(Castling),
    EnPassant(EnPassant),
}

impl Move {
    pub fn attacks_position(&self, pos: &Position) -> bool {
        return match self {
            Move::Quiet(..) => false,
            Move::Promotion(_) => false,
            Move::Castling(_) => false,
            Move::Capture(Capture { new_position, .. }) => new_position == pos,
            Move::EnPassant(EnPassant { captured_pawn, .. }) => captured_pawn == pos,
        };
    }

    pub fn as_quiet(self) -> Result<Quiet, Self> {
        return match self {
            Move::Quiet(q) => Ok(q),
            _ => Err(self),
        };
    }

    pub fn as_capture(self) -> Result<Capture, Self> {
        return match self {
            Move::Capture(q) => Ok(q),
            _ => Err(self),
        };
    }

    pub fn as_promotion(self) -> Result<Promotion, Self> {
        return match self {
            Move::Promotion(q) => Ok(q),
            _ => Err(self),
        };
    }

    pub fn as_castling(self) -> Result<Castling, Self> {
        return match self {
            Move::Castling(q) => Ok(q),
            _ => Err(self),
        };
    }

    pub fn as_en_passant(self) -> Result<EnPassant, Self> {
        return match self {
            Move::EnPassant(q) => Ok(q),
            _ => Err(self),
        };
    }
}
