#![deny(clippy::implicit_return)]
#![allow(clippy::needless_return)]

mod controller;
mod model;
mod view;
mod errors;

fn main() {
    println!("Hello, world!");
}
