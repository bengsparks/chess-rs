use crate::controller::piece::Piece;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum EngineError {
    #[error("Out of bounds computation: {lhs:?} op {rhs:?} = {result:?}")]
    OutOfBoundsPositionResult {
        lhs: (i8, i8),
        rhs: (i8, i8),
        result: (i8, i8),
    },

    #[error("Out of bounds computation: {lhs:?} op {rhs:?} = {result:?}")]
    OutOfBoundsPositionScalarResult {
        lhs: (i8, i8),
        rhs: i8,
        result: (i8, i8),
    },

    #[error("Out of bounds position: {position:?}")]
    OutOfBoundsPosition { position: (i8, i8) },

    #[error("Cannot compute positions between {lhs:?} and {rhs:?}")]
    IncompatibleAxis { lhs: (i8, i8), rhs: (i8, i8) },

    #[error("Attempted to compute moves despite being in checkmate")]
    InvalidBoardContext,

    #[error("Attempted to access piece on {position:?}")]
    NoPieceFound { position: (i8, i8) },

    #[error("Failed to compute cache for {piece:?}")]
    CacheFailure { piece: Piece },
}
